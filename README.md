Scope Sante Data
==============================

**Documentation**: [https://has-sante.gitlab.io/public/alpha/scope-sante-data](https://has-sante.gitlab.io/public/alpha/scope-sante-data)

**Code source**: [https://gitlab.com/has-sante/public/alpha/scope-sante-data](https://gitlab.com/has-sante/public/alpha/scope-sante-data)

---

> ⚠️ DISCLAIMER : ce projet est en **beta**, la structure des tables de données est susceptible d'être modifiée sans préavis. Le contenu de ces données est également en cours de validation, des modifications peuvent subvenir.

Description du projet
--------
Lancé en novembre 2013, le projet Scope Santé a pour but d'informer le public sur le niveau de qualité et de sécurité des soins des établissements de santé de France.

Le projet Scope Santé Data quant à lui, a pour but d'agréger les données concernées par le périmètre Scope Santé, historique compris.

Ce projet n'utilise que des sources de données publiées en Open Data de sorte que le processus de reconstitution de la base Scope Santé ne nécessite pas d'accès ou d'autorisations particulières pour être rejoué ou modifié.

Les domaines de données traités sont les suivants :
- [FINESS](https://www.data.gouv.fr/en/datasets/finess-extraction-du-fichier-des-etablissements/) : Fichier National des Établissements Sanitaires et Sociaux listant tous les établissements
- [IQSS](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2020/) : Indicateurs de Qualité et de Sécurité des Soins
- [e-Satis](https://www.data.gouv.fr/fr/datasets/indicateurs-de-qualite-et-de-securite-des-soins-recueil-2020/) : Indicateurs de mesure de la satisfaction patient
- [SAE](https://data.drees.solidarites-sante.gouv.fr/explore/dataset/708_bases-statistiques-sae/information/) : Statistique Annuelle des Établissements de santé rendant compte des résultats des enquêtes
- [Certification](https://www.data.gouv.fr/fr/datasets/certification-des-etablissements-de-sante-2020/) : résultats du processus de certification des établissements de santé par la HAS

---

Utiliser les données produites
---------
Si vous souhaitez utiliser la donnée produite grâce à ce projet, voilà quelques ressources utiles :
- [Description générale des données](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/general-description.html)
- [La page du projet sur le portail d'Open Data data.gouv.fr](https://www.data.gouv.fr/fr/datasets/scope-sante-data/)
- [Le modèle de données de la table clef-valeur](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/schemas/scope_sante/valeurs.html)
- [Le modèle de données de la table metadata](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/schemas/scope_sante/metadata.html)
- [Les instructions pour mettre à jour la base de données](https://has-sante.gitlab.io/public/alpha/scope-sante-data/maintenance.html).


---

Comprendre le processus de création de la base
-------
Pour comprendre comment la base est créée, voir [la  page dédiée](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/data-processing-description.html).

Pour avoir une description de l'architecture du répertoire du projet, voir [la page dédiée](https://has-sante.gitlab.io/public/alpha/scope-sante-data/architecture-repertoire.html).


Contribuer au projet
-------
Pour contribuer, voir [notre guide](https://has-sante.gitlab.io/public/alpha/scope-sante-data/development.html).
