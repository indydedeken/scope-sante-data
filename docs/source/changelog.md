# Changelog

```{eval-rst}
.. note::
    Find the official changelog here_.
.. _here: https://gitlab.com/has-sante/public/alpha/scope-sante-data/releases/
```
