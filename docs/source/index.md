```{include} ../../README.md
:relative-docs: docs/
:relative-images:
```


```{toctree}
   :hidden:
   :maxdepth: 1
   :glob:

   Données <data-docs/index>
   Exécution <cli-usage>
   Développement <development>
   License <license>
   Changelog <changelog>
```
