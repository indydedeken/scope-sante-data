Description du mode de production des données
=============================================

Dans cette section sont présentés la documentation du processus qui constitue la base ainsi que les schémas de données.

## Description de l'ETL

Après une première présentation générale de l'ETL, les traitements des différents domaines de données sont détaillés dans les liens ci-dessous.

```{eval-rst}
.. toctree::
    :maxdepth: 1

    ETL
    finess
    certification
    iqss
    esatis
    sae
    scope-sante
```

## Schémas de données

Dans cette partie, le framework Frictionless utilisé par le projet est introduit, puis les schémas de données sont présentés.

### Framework Frictionless

Pour renforcer le contrôle qualité des données produites, il est essentiel de mettre en place des contraintes et des vérifications sur ces dernières.

Pour assurer ces bonnes pratiques de développement, la validation des données est assurée par le [framework Frictionless](https://framework.frictionlessdata.io/) (qui correspond à une version améliorée de goodtables).
Ce framework s'accompagne d'un ensemble de [spécifications](https://specs.frictionlessdata.io/) dont le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/), sur lesquelles s'appuie le projet.


**Exemples d'utilisation du framework frictionless :**
- pour valider le "table schema" associé à *historized_finess* :
```
frictionless validate schemas/finess/historized_finess.json --type schema
```
- pour valider un fichier de données (ici *historized_finess.csv*) sur la base du "table schema" correspondant :
```
frictionless validate --path data/finess/processed/finess/historized_finess.csv --schema schemas/finess/historized_finess.json
```

### Présentation des schémas

Les schémas associés aux différents domaines de données sont décrits et accessibles via les liens ci-dessous :

```{eval-rst}
.. toctree::
    :glob:
    :maxdepth: 1

    schemas/*/*
```

## Mise en Production

🚧 WIP 🚧

### Pipeline CI/CD

🚧 WIP 🚧

La validation des données est réalisée au cours du pipeline de CI/CD en partie seulement et via la suite de tests automatisés. Dans ces tests, seules les 1000 premières lignes de données de chaque fichier sont extraites pour être validées. Une validation de l'intégralité des données est toujours réalisable mais le temps nécessaire à son déroulement est significatif, à savoir une dizaine de minutes.

### Validation Fonctionnelle

🚧 WIP 🚧

Des tests automatisés s'assurent de la cohérence entre le référentiel FINESS et les valeurs du fichier clé-valeur, et procèdent à des validations complémentaires via des analyses exploratoires.
