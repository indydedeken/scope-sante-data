# Données

Cette section regroupe l'ensemble de la documentation autour des données utilisées et produites par ce projet.

```{toctree}
   :maxdepth: 1
   :glob:

   Description des données produites <general-description>
   Mouvements de Recomposition des FINESS <mutation-finess>
   Processus de création de la base <data-processing-description>
   Mises à jour des données <../maintenance>
```
