# Domaine Scope Santé

Après une description générale du domaine Scope Santé, sa production – réalisée en deux temps – est décrite : (i) traitements et agrégations, et (ii) validation des données.  
En fin de section, un lien renvoie aux explications quant à sa mise à jour.

## Description générale

Le domaine Scope Santé est le domaine d'agrégation "final" des données Scope Santé, rassemblant tous les autres domaines ([domaine FINESS](finess.md), [domaine Certification](certification.md), [domaine IQSS](iqss.md), [domaine e-Satis](esatis.md), [domaine SAE](sae.md)). La réalisation de cette partie de l'ETL a donc comme pré-requis l'exécution des pipelines de tous les autres domaines de données.

Comme tous les autres domaines de données ont été construits de façon uniforme (un fichier clé-valeur et un fichier CSV de métadonnées), il s'agit ici d'agréger chaque type de fichiers sur l'ensemble des domaines.

Le premier traitement effectué par le domaine Scope Santé est donc l'agrégation des fichiers clé-valeur des autres domaines. Cette agrégation sera en fin de pipeline sauvegardé dans le fichier `valeurs.csv`.  

C'est également lors de cette étape que nous redescendons toutes les décisions de certification par thématiques d'une démarche sur tous les établissements géographiques liés à cette même démarche. Comme tous les établissements ne réalisent pas forcément l'activité pour laquelle une décision par thématique vient de leur être attribuée, il s'agit ensuite de supprimer les lignes de données pour lesquelles cette redescente ne fait pas sens. Pour ce faire, nous utilisons comme spécification le fichier Gestion des finess et des données (`gestion_des_finess_et_des_donnees_V3.8_en cours.docx`) qui spécifie les critères identifiant les établissement concernés par chaque thématique :
- Droits des patients : tous les établissements sont concernés
- Parcours des patients : tous les établissements sont concernés
- Médicaments : tous les établissements sont concernés
- Urgences : l’indicateur est à afficher si et seulement si l’établissement a répondu « Oui » à la question A16 sur l’autorisation de structure d'urgences dans le bordereau « FILTRE » de la SAE
- Bloc opératoire : l’indicateur est à afficher si et seulement si la réponse est « Oui » à la question A18 sur l’activité de bloc opératoire dans le bordereau « FILTRE » de la SAE
- Radiothérapie : l’indicateur est à afficher si et seulement si la réponse est « Oui » à la question A9 concernant les activités cliniques dans le bordereau « FILTRE » de la SAE
- Endoscopie : l’indicateur est à afficher si et seulement si la case A5 ou A14 ou B14 est strictement supérieure à 0 (>0) dans la partie « Description des salles d’intervention et/ou d’exploitation de l’établissement » du bordereau « BLOCS – Sites opératoires et salles d’intervention » de la SAE
- Imagerie interventionnelle : l’indicateur est à afficher si et seulement si la réponse est « Oui » à la question A34 sur l’activité interventionnelle sous imagerie médicale, par voie endovasculaire, en neuroradiologie soumise à autorisation dans le bordereau « FILTRE » de la SAE
- Salle de naissance : l’indicateur est à afficher si et seulement si l’établissement a des activités de maternité
- Management stratégique : tous les établissements sont concernés
- Qualité de vie au travail : tous les établissements sont concernés
- Management de la qualité et des risques : tous les établissements sont concernés
- Risque infectieux : tous les établissements sont concernés
- Douleur : tous les établissements sont concernés
- Fin de vie : l’indicateur est à afficher uniquement pour les établissements MCO, SSR, SLD et HAD
- Dossier patient : tous les établissements sont concernés
- Identification du patient : l’indicateur est à afficher uniquement pour les établissements MCO, SSR, PSY et SLD
- Biologie : l’indicateur est à afficher si et seulement si la réponse est « Oui » à la question A20 sur l’activité de biologie médicale dans le bordereau « FILTRE » de la SAE
- Imagerie : l’indicateur est à afficher si et seulement si la réponse est « Oui » à la question A19 sur l’activité d’imagerie dans le bordereau « FILTRE » de la SAE
- Don d’organes et de tissus : l’indicateur est à afficher uniquement pour les établissements MCO
- Ressources humaines : tous les établissements sont concernés
- Ressources financières : tous les établissements sont concernés
- Système d’information : tous les établissements sont concernés
- Processus logistiques : tous les établissements sont concernés

Dans la pratique cette liste n'est pas complètement exhaustive des thématiques de certification existantes. Dans ce cas, le comportement par défaut qui a été implémenté consiste à considérer que tous les établissements sont concernés par une thématique pour laquelle nous n'avons pas d'instructions particulières.

## Traitements et agrégation des données

Cette phase est décomposée en 3 phases décrites par la suite :

1. Agrégation et attribution des certifications
2. Référentiel FINESS
3. Agrégation des métadonnées et des nomenclatures

### Agrégation et attribution des certifications

Le premier traitement effectué par le pipeline du domaine Scope Santé est l'agrégation des fichiers clé-valeur des autres domaines. Cette agrégation sera en fin de pipeline sauvegardée dans le fichier `data/scope_sante/final/valeurs.csv`.  

C'est également lors de cette étape que toutes les décisions de certification par thématiques associées à une Entité Juridique (EJ) sont rattachés aux établissements géographiques (ET) liés. Comme tous les établissements ne réalisent pas forcément l'activité correspondant à une décision par thématique donnée, il s'agit ensuite de supprimer les lignes de données associées à de telles redescentes inadaptées. Pour ce faire, le fichier *Gestion des FINESS et des données* (`gestion_des_finess_et_des_donnees_V3.8_en cours.docx`) spécifie les critères identifiant les établissement concernés par chaque thématique. En pratique, le fichier ne couvre pas exhaustivement les thématiques de certification. En cas de thématique manquante, le comportement par défaut qui a été implémenté consiste à considérer que tous les établissements sont concernés par la thématique.

### Référentiel FINESS

Le second traitement est la construction du référentiel FINESS qui est sauvegardé en fin de pipeline dans le fichier `data/scope_sante/final/finess.csv`. Ce référentiel est un sous-ensemble du référentiel FINESS produit par le domaine FINESS. Les filtres appliqués ont une double origine.

D'une part, ils sont liés aux autorisations d'activités de soins. Les règles appliquées pour ces filtres sont celles référencées dans le document de *Gestion des FINESS et des données* (`gestion_des_finess_et_des_donnees_V3.8_en cours.docx`). À savoir, seuls les établissements disposant d'au moins une autorisation d’activités de soins parmi les suivantes sont affichés dans Scope Santé :
- Médecine
- Chirurgie
- Gynécologie/Obstétrique
- Psychiatrie (en hospitalisation complète)
- Soins de longue durée
- Soins de suite et de réadaptation
- Hospitalisation à domicile.

D'autre part, l'autre règle de filtrage s'appuie sur le fichier `data/scope_sante/final/valeurs.csv` résultant de l'agrégation des fichiers clé-valeur. En effet, certains numéro FINESS ne sont pas présents dans ce fichier et sont donc exclus du référentiel FINESS, afin de garantir la spécificité de ce référentiel.  

### Agrégation des métadonnées

Le troisième et dernier traitement important concerne l'agrégation des fichiers de métadonnées et de nomenclatures. Comme pour les fichiers clé-valeur, chaque domaine dispose d'un fichier de métadonnées. L'ensemble de ces fichiers est agrégé au sein du domaine Scope Santé pour donner le fichier `data/scope_sante/final/metadata.csv` en sortie de pipeline. Le procédé est similaire pour la table de nomenclatures qui est sauvegardée sous le nom de `data/scope_sante/final/nomenclatures.csv`.

À noter qu'il existe un cinquième et dernier fichier en sortie du pipeline Scope Santé : `data/scope_sante/final/autorisations_as.csv`. Aucune transformation n'est réalisée sur ce fichier par rapport à son état en sortie du pipeline du domaine FINESS.

## Validation des données

La validation des données du domaine Scope Santé est réalisée par la librairie [frictionless](https://framework.frictionlessdata.io/docs/guides/introduction/) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/). Cette validation s'appuie sur 2 fichiers de métadonnées (un pour chaque table concernée) au format JSON et se trouvant dans le dossier `schemas/scope_sante` : `metadata.json` et `valeurs.json`.

## Mise à jour du périmètre Scope Santé

Pour mettre à jour le domaine Scope Santé, suivre la procédure décrite [ici](../maintenance.md).
