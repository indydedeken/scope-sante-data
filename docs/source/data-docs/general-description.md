Description générale des données
================================

Dans cette section, après une description globale du modèle de données final, une description plus détaillée des tables metadata et valeurs est effectuée.

Modèle de données final
-----------------------

La base de données finale s'articule autour de 5 tables :
- finess : la table finess servant de référentiel
- autorisations_as : la table des autorisations d'activités de soins
- valeurs : la table contenant toutes les variables au format clé-valeur
- metadata : la table documentant chacune des clés utilisées dans la table valeurs
- nomenclatures : la table documentant la nomenclature des clés de données

Le modèle relationnel de ces 4 tables est décrit sur le diagramme ci-dessous :

```{eval-rst}
.. mermaid::

  erDiagram
    autorisations_as {
        date dateexport
        string num_finess_et
        string num_finess_ej
        int activite
        int modalite
        int forme
        date datemeo
        date datefin
    }
    finess {
        date dateexport
        string num_finess_et
        string num_finess_ej
        string rs
        string commune
        string departement
        boolean ferme_cette_annee
        boolean actif_scope_sante
        boolean dernier_enregistrement
    }
    valeurs {
        int annee
        string finess
        string finess_type
        string key
        boolean value_boolean
        string value_string
        int value_integer
        float value_float
    }
    metadata {
        string name
        string title
        string description
        string type
        string source
    }
    nomenclatures {
        string key
        string value
        string label
        string source
    }

    autorisations_as ||--o{ finess : "num_finess_et:num_finess_et"
    valeurs ||--o{ finess : "num_finess_{et/ej}:finess"
    metadata ||--o{ valeurs : "key:name"
    metadata ||--o{ valeurs : "key:name"
    nomenclatures ||--o{ metadata : "name:key"
```

Ainsi, dans la table valeurs constituant un tableau associatif, la valeur de chaque variable est associée à une clé documentée dans la table de metadata. Ce modèle permet une très grande souplesse quant aux évolutions futures.

Chaque valeur est également liée à un numéro FINESS (référencé dans la table finess), qu'il soit géographique ou juridique  – ce type étant renseigné dans la colonne `finess_type` de la table valeurs.

Les données d'autorisations et de FINESS sont issues du domaine FINESS décrit dans la section "Description Générale" de la page [Domaine FINESS](./finess.md).
Leurs mutations sont décrites dans la page [Mouvements de Recompostion](./mutation-finess.md).

Les données de la table `valeurs` proviennent de quatre domaines :
- le domaine SAE (voir section [Description Générale](./sae.md) correspondante)
- le domaine Certifications (voir section [Description Générale](./certification.md) correspondante)
- le domaine e-Satis (voir section [Description Générale](./esatis.md) correspondante)
- le domaine IQSS (voir section [Description Générale](./iqss.md) correspondante)


Table metadata
--------------

La table metadata a pour but de documenter les clés de la table valeurs. Outre le nom de la clé, on y retrouve une description ainsi que le type de la donnée associée.
La source d'où est issue la donnée brute est également présente ainsi que le secteur auquel la clé peut être associée.
Pour les clés associées aux variables IQSS, des champs spécifiques sont également documentés dont le thème et l'URL du rapport de l'indicateur.

Le modèle de la table metadata est disponible via le lien ci-dessous :

```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/scope_sante/metadata
```

Table valeurs
-------------

La table valeurs contient toutes les valeurs des variables des domaines suivants :
- IQSS
- e-Satis
- SAE
- Certification

Pour pouvoir être importées dans une base relationnelle, les types primitifs des valeurs doivent pouvoir être explicitement identifiés (booléen, chaîne de caractères, entier et décimal – en anglais boolean, string, integer et float). Pour cela, il existe 4 colonnes qui correspondent à chacun des types de valeurs supportés par la plupart des SGBDR, et dans lesquels se répartissent les valeurs selon qu'il s'agisse de booléen (boolean), de chaîne de caractères (string), d'entier (integer) ou de décimal (float).

Le modèle de la table valeurs est disponible ici :

```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/scope_sante/valeurs
```

Table nomenclatures
-------------

La table nomenclatures contient les correspondances valeur-libellés pour chaque couple clé-valeurs concernant une variable catégorielle.

Le modèle de la table nomenclatures est disponible ici :

```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/scope_sante/nomenclatures
```
