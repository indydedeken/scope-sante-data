#! /usr/bin/env python
import logging

import click

from scope_sante_data.certification import constants as certification_constants
from scope_sante_data.certification.main import main as certification_main
from scope_sante_data.constants import LOG_LEVEL
from scope_sante_data.esatis import constants as esatis_constants
from scope_sante_data.esatis.main import main as esatis_main
from scope_sante_data.finess import constants as finess_constants
from scope_sante_data.finess.main import main as finess_main
from scope_sante_data.iqss import constants as iqss_constants
from scope_sante_data.iqss.main import main as iqss_main
from scope_sante_data.sae import constants as sae_constants
from scope_sante_data.sae.main import main as sae_main
from scope_sante_data.scope_sante.main import main as scope_sante_main

logging.basicConfig(
    level=logging.getLevelName(LOG_LEVEL),
    format="%(asctime)s %(name)-12s %(levelname)-8s %(message)s",
)


@click.group()
def cli():
    pass


@cli.command()
@click.version_option(
    prog_name="scope_sante_data", help="Affiche la version et quitte."
)
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def finess(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données FINESS"""
    if (
        not skip_data_dl
        and finess_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {finess_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True
    finess_main(
        skip_data_download=skip_data_dl, data_validation=data_validation
    )


@cli.command()
@click.version_option(
    prog_name="scope_sante_data", help="Affiche la version et quitte."
)
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def certification(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données Certification"""
    if (
        not skip_data_dl
        and certification_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {certification_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    certification_main(
        skip_data_download=skip_data_dl,
        data_validation=data_validation,
    )


@cli.command()
@click.version_option(
    prog_name="scope_sante_data", help="Affiche la version et quitte."
)
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def sae(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données SAE"""
    if (
        not skip_data_dl
        and sae_constants.DATA_DOMAIN_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {sae_constants.DATA_DOMAIN_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    sae_main(skip_data_download=skip_data_dl, data_validation=data_validation)


@cli.command()
@click.version_option(
    prog_name="scope_sante_data", help="Affiche la version et quitte."
)
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def iqss(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données IQSS"""
    if (
        not skip_data_dl
        and iqss_constants.DATA_IQSS_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {iqss_constants.DATA_IQSS_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    iqss_main(skip_data_download=skip_data_dl, data_validation=data_validation)


@cli.command()
@click.version_option(
    prog_name="scope_sante_data", help="Affiche la version et quitte."
)
@click.option(
    "--skip-data-dl",
    default=False,
    is_flag=True,
    help="Désactive le téléchargement des données",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données en fin de pipeline",
)
def esatis(skip_data_dl=False, data_validation=False) -> None:
    """Lance le pipeline du domaine de données e-Satis"""
    if (
        not skip_data_dl
        and esatis_constants.DATA_ESATIS_PATH.exists()
        and not click.confirm(
            f"Des données existent déjà dans {esatis_constants.DATA_ESATIS_PATH},"
            "êtes-vous sûr de vouloir les écraser ?"
        )
    ):
        skip_data_dl = True

    esatis_main(
        skip_data_download=skip_data_dl, data_validation=data_validation
    )


@cli.command()
@click.version_option(
    prog_name="scope_sante_data", help="Affiche la version et quitte."
)
@click.option(
    "--force-full-run",
    default=False,
    is_flag=True,
    help="Force le run de tous les pipelines nécessaires à la constitution de la base",
)
@click.option(
    "--data-validation",
    default=False,
    is_flag=True,
    help="Active la validation des données pour chaque domaine de données intermédiaire",
)
@click.option(
    "--open-data",
    default=False,
    is_flag=True,
    help="Active l'update des données en open data (data.gouv.fr)",
)
def scope_sante(
    force_full_run=False,
    data_validation=False,
    open_data=False,
) -> None:
    """Lance la constitution de la base Scope Santé"""
    scope_sante_main(
        force_full_run=force_full_run,
        data_validation=data_validation,
        open_data=open_data,
    )


if __name__ == "__main__":
    cli()
