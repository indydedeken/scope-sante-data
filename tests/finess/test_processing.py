from typing import Final

import pytest

from scope_sante_data import data_acquisition_utils
from scope_sante_data.finess import (
    processing_autorisations_as,
    processing_finess,
    processing_finess_ej,
)
from scope_sante_data.finess.constants import (
    DATA_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
)
from scope_sante_data.finess.data_acquisition import download_data
from scope_sante_data.finess.processing import clean_directory_tree

LATEST_FINESS_COLUMNS: Final[list] = [
    "categagretab",
    "categetab",
    "codeape",
    "codemft",
    "codesph",
    "commune",
    "compldistrib",
    "complrs",
    "compvoie",
    "coordxet",
    "coordyet",
    "dateautor",
    "date_export",
    "dategeocod",
    "datemaj",
    "dateouv",
    "departement",
    "libcategagretab",
    "libcategetab",
    "libdepartement",
    "libmft",
    "libsph",
    "lieuditbp",
    "ligneacheminement",
    "nofinessej",
    "nofinesset",
    "numuai",
    "numvoie",
    "rs",
    "rslongue",
    "siret",
    "sourcecoordet",
    "telecopie",
    "telephone",
    "typvoie",
    "voie",
]

HISTORIZED_FINESS_COLUMNS: Final[list] = [
    "date_export",
    "num_finess_et",
    "num_finess_ej",
    "raison_sociale",
    "raison_sociale_longue",
    "complement_raison_sociale",
    "complement_distribution",
    "num_voie",
    "type_voie",
    "libelle_voie",
    "complement_voie",
    "lieu_dit_bp",
    "commune",
    "departement",
    "libelle_departement",
    "ligne_acheminement",
    "telephone",
    "telecopie",
    "categorie_et",
    "libelle_categorie_et",
    "categorie_agregat_et",
    "libelle_categorie_agregat_et",
    "siret",
    "code_ape",
    "code_mft",
    "libelle_mft",
    "code_sph",
    "libelle_sph",
    "date_ouverture",
    "date_autorisation",
    "date_maj",
    "num_uai",
    "coord_x_et",
    "coord_y_et",
    "source_coord_et",
    "date_geocodage",
    "region",
    "libelle_region",
    "code_officiel_geo",
    "code_postal",
    "libelle_routage",
    "libelle_code_ape",
    "ferme_cette_annee",
    "latitude",
    "longitude",
    "libelle_commune",
    "adresse_postale_ligne_1",
    "adresse_postale_ligne_2",
]

LATEST_AUTORISATIONS_AS_COLUMNS: Final[list] = [
    "activite",
    "dateautor",
    "date_export",
    "datefin",
    "datemeo",
    "forme",
    "libactivite",
    "libforme",
    "libmodalite",
    "libsectpsy",
    "modalite",
    "noautor",
    "noautorarhgos",
    "nofinessej",
    "nofinesset",
    "noimplarhgos",
    "noligautor",
    "noligautoranc",
    "rsej",
    "rset",
    "sectpsy",
]

HISTORIZED_AUTORISATIONS_AS_COLUMNS: Final[list] = [
    "date_export",
    "num_finess_ej",
    "raison_sociale_ej",
    "activite",
    "libelle_activite",
    "modalite",
    "libelle_modalite",
    "forme",
    "libelle_forme",
    "num_autorisation",
    "date_autorisation",
    "num_finess_et",
    "raison_sociale_et",
    "num_ligne_autorisee",
    "ancien_num_ligne_autorisee",
    "num_autorisation_arhgos",
    "num_implantation_arhgos",
    "code_secteur_psy",
    "libelle_secteur_psy",
    "date_mise_en_oeuvre",
    "date_fin",
    "ancien_num_autorisation",
    "date_maj_autorisation",
    "organe",
    "libelle_organe",
    "num_mise_en_oeuvre",
    "date_maj_mise_en_oeuvre",
    "appartenance_cartesan",
    "capacite_autorisee",
]


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download():
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_DOMAIN_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)


def test_build_latest_finess_data():
    clean_directory_tree()

    finess_df = processing_finess.build_latest_data()

    assert finess_df.shape[0] > 10000
    assert finess_df.shape[1] == 36
    assert set(LATEST_FINESS_COLUMNS) == set(finess_df.columns)


def test_build_historized_finess_data():
    clean_directory_tree()

    historized_finess_df = processing_finess.build_historized_finess_data()

    assert historized_finess_df.shape[0] > 200_000
    assert historized_finess_df.shape[1] == 48
    assert set(HISTORIZED_FINESS_COLUMNS) == set(historized_finess_df.columns)


def test_build_latest_finess_ej_data():
    clean_directory_tree()

    finess_df = processing_finess_ej.build_latest_data()

    assert finess_df.shape[0] > 10000
    assert finess_df.shape[1] == 23


def test_build_historized_finess_ej_data():
    clean_directory_tree()

    historized_finess_df = (
        processing_finess_ej.build_historized_finess_ej_data()
    )

    assert historized_finess_df.shape[0] > 1e6
    assert historized_finess_df.shape[1] == 31


def test_build_latest_autorisations_as_data():
    clean_directory_tree()

    autorisations_as_df = processing_autorisations_as.build_latest_data()

    assert autorisations_as_df.shape[0] > 10000
    assert autorisations_as_df.shape[1] == 21
    assert set(LATEST_AUTORISATIONS_AS_COLUMNS) == set(
        autorisations_as_df.columns
    )


def test_build_historized_autorisations_as_data():
    clean_directory_tree()

    historized_autorisations_as_df = (
        processing_autorisations_as.build_historized_autorisations_as_data()
    )

    assert historized_autorisations_as_df.shape[0] > 3e5
    assert historized_autorisations_as_df.shape[1] == 29
    assert set(HISTORIZED_AUTORISATIONS_AS_COLUMNS) == set(
        historized_autorisations_as_df.columns
    )
