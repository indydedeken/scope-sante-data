import os

from scope_sante_data import data_acquisition_utils
from scope_sante_data.finess.constants import (
    RAW_AUTORISATIONS_AS_PATH,
    RAW_FINESS_EJ_PATH,
    RAW_FINESS_ET_PATH,
    RESOURCES_DOMAIN_PATH,
)
from scope_sante_data.finess.data_acquisition import download_data


def test_download_data():
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )

    download_data(data_sources)

    # finess ET
    assert len(os.listdir(RAW_FINESS_ET_PATH)) > 0
    assert os.path.exists(RAW_FINESS_ET_PATH / "latest.csv")
    assert os.path.exists(RAW_FINESS_ET_PATH / "historic.zip")
    assert os.stat(RAW_FINESS_ET_PATH / "latest.csv").st_size > 0
    assert os.stat(RAW_FINESS_ET_PATH / "historic.zip").st_size > 0
    # finess EJ
    assert len(os.listdir(RAW_FINESS_EJ_PATH)) > 0
    assert os.path.exists(RAW_FINESS_EJ_PATH / "latest.csv")
    assert os.path.exists(RAW_FINESS_EJ_PATH / "historic.zip")
    assert os.stat(RAW_FINESS_EJ_PATH / "latest.csv").st_size > 0
    assert os.stat(RAW_FINESS_EJ_PATH / "historic.zip").st_size > 0
    # autorisation activités de soins
    assert len(os.listdir(RAW_AUTORISATIONS_AS_PATH)) > 0
    assert os.path.exists(RAW_AUTORISATIONS_AS_PATH / "latest.csv")
    assert os.path.exists(RAW_AUTORISATIONS_AS_PATH / "historic.zip")
    assert os.stat(RAW_AUTORISATIONS_AS_PATH / "latest.csv").st_size > 0
    assert os.stat(RAW_AUTORISATIONS_AS_PATH / "historic.zip").st_size > 0
