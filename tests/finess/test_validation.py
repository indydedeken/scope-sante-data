from itertools import islice

import pandas as pd
import pytest
from frictionless import Schema

from scope_sante_data.constants import UTF_8
from scope_sante_data.finess.constants import (
    FINAL_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from scope_sante_data.finess.processing import process_data
from scope_sante_data.finess.processing_finess import FINAL_FINESS_DTYPES_DICT
from scope_sante_data.validation_utils import is_data_valid, is_metadata_valid

# import needed for its data_download fixture
# noinspection PyUnresolvedReferences
from tests.finess.test_processing import (  # pylint: disable=unused-import
    fixture_data_download,
)


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing():
    process_data()

    # create a subset of the data to validate
    with open(
        FINAL_DOMAIN_PATH / "finess_extract.csv", "w"
    ) as finess_output_extract, open(
        FINAL_DOMAIN_PATH / "finess.csv"
    ) as finess_output:
        finess_output_extract.writelines(
            line for line in islice(finess_output, 1000)
        )

    with open(
        FINAL_DOMAIN_PATH / "autorisations_as_extract.csv",
        "w",
    ) as finess_output_extract, open(
        FINAL_DOMAIN_PATH / "autorisations_as.csv"
    ) as finess_output:
        finess_output_extract.writelines(
            line for line in islice(finess_output, 1000)
        )


def test_validate_finess_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "finess.json")

    assert is_metadata_valid(schema)


def test_validate_autorisations_as_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "autorisations_as.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_finess_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "finess_extract.csv",
        SCHEMAS_DOMAIN_PATH / "finess.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_autorisations_as_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "autorisations_as_extract.csv",
        SCHEMAS_DOMAIN_PATH / "autorisations_as.json",
    )


@pytest.mark.skip(reason="FINESS output validation takes about 10mn")
@pytest.mark.usefixtures("data_processing")
def test_validate_full_finess_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "finess.csv",
        SCHEMAS_DOMAIN_PATH / "finess.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_latitude_longitude():
    finess_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_FINESS_DTYPES_DICT,
    )

    invalid_finess = finess_df[
        (finess_df["latitude"] < -90)
        | (finess_df["latitude"] > 90)
        | (finess_df["longitude"] < -180)
        | (finess_df["longitude"] > 180)
    ]

    assert (
        invalid_finess.empty
    ), f"There are {invalid_finess['nofinesset'].nunique()} FINESS with invalid latitude/longitude"
