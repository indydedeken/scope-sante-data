import pytest
from frictionless import Schema

from scope_sante_data.scope_sante.constants import (
    FINAL_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from scope_sante_data.validation_utils import is_data_valid, is_metadata_valid


def test_validate_table_schemas():
    metadata_schema = Schema(SCHEMAS_DOMAIN_PATH / "metadata.json")
    valeurs_schema = Schema(SCHEMAS_DOMAIN_PATH / "valeurs.json")
    nomenclatures_schema = Schema(SCHEMAS_DOMAIN_PATH / "nomenclatures.json")

    assert is_metadata_valid(metadata_schema)
    assert is_metadata_valid(valeurs_schema)
    assert is_metadata_valid(nomenclatures_schema)


@pytest.mark.run("last")
def test_validate_valeurs_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        SCHEMAS_DOMAIN_PATH / "valeurs.json",
    )


def test_validate_metadata_content():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "metadata.csv",
        SCHEMAS_DOMAIN_PATH / "metadata.json",
    )


def test_validate_nomenclatures_content():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "nomenclatures.csv",
        SCHEMAS_DOMAIN_PATH / "nomenclatures.json",
    )
