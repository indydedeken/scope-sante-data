import logging
import os
from typing import Final

import pandas as pd
import pytest
from pytest_mock import MockerFixture
from requests.models import Response

from scope_sante_data import data_acquisition_utils
from scope_sante_data.constants import FINESS_TYPE_GEO, FINESS_TYPE_JUR, UTF_8
from scope_sante_data.data_acquisition_utils import clean_directory
from scope_sante_data.finess import constants as finess_constants
from scope_sante_data.finess.processing_finess import FINAL_FINESS_DTYPES_DICT
from scope_sante_data.scope_sante.constants import (
    FINAL_DOMAIN_PATH,
    KEY_VALUE_DTYPES_DICT,
    RESOURCES_DOMAIN_PATH,
)
from scope_sante_data.scope_sante.open_data import (
    update_metadata,
    upload_open_data,
)
from scope_sante_data.scope_sante.processing import process_data

logger: Final[logging.Logger] = logging.getLogger(__name__)


@pytest.mark.run("second-to-last")
def test_process_data():
    clean_directory(FINAL_DOMAIN_PATH)

    process_data()

    assert os.path.exists(FINAL_DOMAIN_PATH / "autorisations_as.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "finess.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "metadata.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "metadata.xlsx")
    assert os.path.exists(FINAL_DOMAIN_PATH / "valeurs.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "nomenclatures.csv")
    assert os.path.exists(FINAL_DOMAIN_PATH / "nomenclatures.xlsx")


@pytest.mark.run("last")
def test_validate_finess_join():
    finess_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_FINESS_DTYPES_DICT,
    )
    all_finess = pd.concat(
        [finess_df["num_finess_et"], finess_df["num_finess_ej"]]
    )
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )

    missing_finess_df = valeurs_df[~valeurs_df["finess"].isin(all_finess)]

    assert (
        missing_finess_df.empty
    ), f"""Ces finess ne sont pas référencés : {missing_finess_df["finess"].unique().to_numpy()}"""


@pytest.mark.run("last")
def test_validate_finess_type():
    finess_df = pd.read_csv(
        finess_constants.FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_FINESS_DTYPES_DICT,
    )
    valeurs_df = pd.read_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )

    valeurs_finess_jur = valeurs_df[
        valeurs_df["finess_type"] == FINESS_TYPE_JUR
    ]
    valeurs_wrong_finess = valeurs_finess_jur[
        ~valeurs_finess_jur["finess"].isin(finess_df["num_finess_ej"])
    ]
    assert valeurs_wrong_finess.empty, (
        "Des finess parmi les clés suivantes ne sont pas juridiques (ou non-référencés) : %s"
        % valeurs_wrong_finess["key"].unique().tolist()
    )

    valeurs_finess_geo = valeurs_df[
        valeurs_df["finess_type"] == FINESS_TYPE_GEO
    ]
    valeurs_wrong_finess = valeurs_finess_geo[
        ~valeurs_finess_geo["finess"].isin(finess_df["num_finess_et"])
    ]
    assert valeurs_wrong_finess.empty, (
        "Des finess parmi des clé suivantes ne sont pas géographiques (ou non-référencés) : %s"
        % valeurs_wrong_finess["key"].unique().tolist()
    )


@pytest.mark.run("last")
def test_update_metadata(mocker: MockerFixture):
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )
    ok_response = Response()
    ok_response.status_code = 200
    mocker.patch(
        "scope_sante_data.data_gouv_utils.requests.put",
        return_value=ok_response,
    )

    update_metadata(data_sources)


@pytest.mark.run("last")
def test_update_data(mocker: MockerFixture):
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )
    ok_response = Response()
    ok_response.status_code = 200
    mocker.patch(
        "scope_sante_data.data_gouv_utils.requests.post",
        return_value=ok_response,
    )

    upload_open_data(data_sources)
