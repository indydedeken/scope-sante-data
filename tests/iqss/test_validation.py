from itertools import islice

import pytest
from frictionless import Schema

from scope_sante_data.iqss.constants import (
    CLEAN_IQSS_PATH,
    FINAL_IQSS_PATH,
    SCHEMAS_IQSS_PATH,
)
from scope_sante_data.iqss.processing import process_data
from scope_sante_data.validation_utils import is_data_valid, is_metadata_valid

# import needed for its data_download fixture
# noinspection PyUnresolvedReferences
from tests.iqss.test_processing import (  # pylint: disable=unused-import
    fixture_data_download,
)


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing():
    process_data()

    # create a subset of the data to validate
    with open(
        CLEAN_IQSS_PATH / "iqss_output_extract.csv", "w"
    ) as iqss_output_extract, open(
        FINAL_IQSS_PATH / "iqss_key_value.csv"
    ) as iqss_output:
        iqss_output_extract.writelines(
            line for line in islice(iqss_output, 1000)
        )


def test_validate_iqss_metadata():
    schema = Schema(SCHEMAS_IQSS_PATH / "iqss_key_value.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_iqss_data():
    assert is_data_valid(
        CLEAN_IQSS_PATH / "iqss_output_extract.csv",
        SCHEMAS_IQSS_PATH / "iqss_key_value.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_full_iqss_data():
    assert is_data_valid(
        FINAL_IQSS_PATH / "iqss_key_value.csv",
        SCHEMAS_IQSS_PATH / "iqss_key_value.json",
    )
