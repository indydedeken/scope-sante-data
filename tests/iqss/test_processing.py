import logging
from typing import Final

import pandas as pd
import pytest

from scope_sante_data import data_acquisition_utils
from scope_sante_data.constants import (
    FINESS_TYPE_GEO,
    FINESS_TYPE_JUR,
    FINESS_TYPE_UNKNOWN,
)
from scope_sante_data.data_acquisition_utils import clean_directory
from scope_sante_data.iqss.constants import (
    DATA_IQSS_PATH,
    FINAL_IQSS_PATH,
    RESOURCES_IQSS_PATH,
)
from scope_sante_data.iqss.data_acquisition import download_data
from scope_sante_data.iqss.processing import merged_iqss_data
from scope_sante_data.iqss.processing_clean import clean_data
from scope_sante_data.iqss.processing_finess import set_finess_type
from scope_sante_data.iqss.processing_reformate import (
    reformate_historized_iqss,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)
HISTORIZED_IQSS_COLUMNS: Final[list] = [
    "finess",
    "finess_type",
    "raison_sociale",
    "annee",
    "key",
    "value_float",
    "value_string",
]


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download():
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_IQSS_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_IQSS_PATH
        )
        download_data(data_sources)


@pytest.mark.skip(
    reason="Certains indicateurs du thème DPA des receuils 2019 et 2020 ne sont pas remontés qu'au niveau géographique, à investiguer"
)
def test_build_historized_iqss_data():
    clean_directory(FINAL_IQSS_PATH)

    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv", sep=",")
    clean_data(metadata_df)
    merged_iqss_data()
    set_finess_type()
    historized_iqss_key_value = reformate_historized_iqss()

    assert historized_iqss_key_value.shape[0] > 10000
    assert historized_iqss_key_value.shape[1] == 7
    assert (
        len(
            set(historized_iqss_key_value["key"])
            - set(metadata_df["code"].str.lower())
        )
        == 0
    )
    assert set(HISTORIZED_IQSS_COLUMNS) == set(
        historized_iqss_key_value.columns
    )

    # Validate type finess
    # Pour les indicateurs ete | iso avant 2019, le finess est mixte (mélange de
    # geo et jur). A partir de 2019, le finess est géographique uniquement
    iso_ete_after_2019 = historized_iqss_key_value[
        historized_iqss_key_value["key"].str.contains("isoortho|eteortho")
        & (historized_iqss_key_value["annee"] >= 2019)
    ]
    assert (
        iso_ete_after_2019["finess_type"]
        .isin([FINESS_TYPE_GEO, FINESS_TYPE_UNKNOWN])
        .all()
    ), "Certains indicateurs ete/iso ne sont pas remontés qu'au niveau géographique"

    # Pour les indicateurs du thèmes DPA des recueil 2019 et 2020 le finess doit
    # être géographique
    dpa_2018_2019 = historized_iqss_key_value[
        historized_iqss_key_value["annee"].isin([2018, 2019])
        & historized_iqss_key_value["key"].str.contains("dpa")
    ]
    assert (
        dpa_2018_2019["finess_type"]
        .isin([FINESS_TYPE_GEO, FINESS_TYPE_UNKNOWN])
        .all()
    ), "Certains indicateurs du thème DPA des receuils 2019 et 2020 ne sont pas remontés qu'au niveau géographique"

    # Le type de finess des indicateurs psy et icsha doit être mixte (geo + jur)
    psy_icsha_finess_types = historized_iqss_key_value[
        historized_iqss_key_value["key"].str.contains("psy|icsha")
    ]["finess_type"].unique()
    assert all(
        finess_type in psy_icsha_finess_types
        for finess_type in [FINESS_TYPE_GEO, FINESS_TYPE_JUR]
    ), "Le type de finess des indicateurs psy/icsha devrait être mixte (géo et jur)"
