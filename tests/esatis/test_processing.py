import logging
from typing import Final

import pytest

from scope_sante_data import data_acquisition_utils
from scope_sante_data.data_acquisition_utils import clean_directory
from scope_sante_data.esatis.constants import (
    DATA_ESATIS_PATH,
    FINAL_ESATIS_PATH,
    RESOURCES_ESATIS_PATH,
)
from scope_sante_data.esatis.data_acquisition import download_data
from scope_sante_data.esatis.processing import (
    clean_data,
    merged_data,
    reformate_historized,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)
HISTORIZED_ESATIS_COLUMNS: Final[list] = [
    "finess",
    "finess_type",
    "annee",
    "key",
    "value_float",
    "value_string",
]


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download():
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_ESATIS_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_ESATIS_PATH
        )
        download_data(data_sources)


def test_build_historized_esatis_key_value_data():
    clean_directory(FINAL_ESATIS_PATH)

    clean_data()
    merged_data()
    esatis_key_value = reformate_historized()

    assert esatis_key_value.shape[0] > 10000
    assert esatis_key_value.shape[1] == 6
    assert set(HISTORIZED_ESATIS_COLUMNS) == set(esatis_key_value.columns)
