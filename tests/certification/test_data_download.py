import os

from scope_sante_data import data_acquisition_utils
from scope_sante_data.certification.constants import (
    RAW_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
)
from scope_sante_data.certification.data_download import download_data


def test_download_data():
    data_sources = data_acquisition_utils.load_data_sources_file(
        RESOURCES_DOMAIN_PATH
    )

    download_data(data_sources)

    assert len(os.listdir(RAW_DOMAIN_PATH)) > 0
    for filename in [
        "certifications.csv",
        "thematiques.csv",
        "thematiques_labels.csv",
    ]:
        assert os.path.exists(RAW_DOMAIN_PATH / filename)
        assert os.stat(RAW_DOMAIN_PATH / filename).st_size > 0
