from itertools import islice

import pytest
from frictionless import Schema

from scope_sante_data.certification.constants import (
    FINAL_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from scope_sante_data.certification.processing import process_data
from scope_sante_data.validation_utils import is_data_valid, is_metadata_valid

# import needed for its data_download fixture
# noinspection PyUnresolvedReferences
from tests.certification.test_processing import (  # pylint: disable=unused-import
    fixture_data_download,
)


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing():
    process_data()

    # create a subset of the data to validate
    with open(
        FINAL_DOMAIN_PATH / "certifications_extract.csv", "w"
    ) as certifications_extract, open(
        FINAL_DOMAIN_PATH / "certifications.csv"
    ) as certifications:
        certifications_extract.writelines(
            line for line in islice(certifications, 1000)
        )

    with open(
        FINAL_DOMAIN_PATH / "thematiques_extract.csv", "w"
    ) as thematiques_extract, open(
        FINAL_DOMAIN_PATH / "thematiques.csv"
    ) as thematiques:
        thematiques_extract.writelines(
            line for line in islice(thematiques, 1000)
        )


def test_validate_certifications_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "certifications.json")

    assert is_metadata_valid(schema)


def test_validate_thematiques_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "thematiques.json")

    assert is_metadata_valid(schema)


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_certifications_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "certifications_extract.csv",
        SCHEMAS_DOMAIN_PATH / "certifications.json",
    )


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_thematiques_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "thematiques_extract.csv",
        SCHEMAS_DOMAIN_PATH / "thematiques.json",
    )
