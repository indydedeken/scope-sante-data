import os
from itertools import islice

import pytest
from frictionless import Schema

from scope_sante_data.metadata_utils import table_schema_to_csv
from scope_sante_data.sae.constants import (
    FINAL_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from scope_sante_data.sae.processing import process_data
from scope_sante_data.validation_utils import is_data_valid, is_metadata_valid

# import needed for its data_download fixture
# noinspection PyUnresolvedReferences
from tests.sae.test_processing import (  # pylint: disable=unused-import
    fixture_data_download,
)


@pytest.fixture(name="data_processing", scope="session")
@pytest.mark.usefixtures("data_download")
def fixture_data_processing():
    process_data()

    # create a subset of the data to validate
    with open(FINAL_DOMAIN_PATH / "sae_extract.csv", "w") as sae_extract, open(
        FINAL_DOMAIN_PATH / "sae.csv"
    ) as sae:
        sae_extract.writelines(line for line in islice(sae, 1000))


def test_validate_metadata():
    schema = Schema(SCHEMAS_DOMAIN_PATH / "sae.json")

    assert is_metadata_valid(schema)


def test_table_schema_to_csv():
    if not FINAL_DOMAIN_PATH.exists():
        FINAL_DOMAIN_PATH.mkdir(parents=True)
    table_schema_to_csv(
        SCHEMAS_DOMAIN_PATH / "sae.json",
        FINAL_DOMAIN_PATH / "sae_metadata.csv",
    )

    assert os.path.exists(FINAL_DOMAIN_PATH / "sae_metadata.csv")


@pytest.mark.usefixtures("data_processing")
def test_validate_partial_data():
    assert is_data_valid(
        FINAL_DOMAIN_PATH / "sae_extract.csv",
        SCHEMAS_DOMAIN_PATH / "sae.json",
    )
