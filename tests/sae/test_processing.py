import pytest

from scope_sante_data import data_acquisition_utils
from scope_sante_data.sae.constants import (
    DATA_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
)
from scope_sante_data.sae.data_download import download_data
from scope_sante_data.sae.processing import process_id_data
from scope_sante_data.sae.processing_dialyse import process_dialyse_data
from scope_sante_data.sae.processing_filtre import process_filtre_data
from scope_sante_data.sae.processing_had import process_had_data
from scope_sante_data.sae.processing_mco import process_mco_data
from scope_sante_data.sae.processing_psy import process_psy_data
from scope_sante_data.sae.processing_ssr import process_ssr_data


@pytest.fixture(name="data_download", scope="session", autouse=True)
def fixture_data_download():
    # Since data download is time/network consuming, we only do it if they have
    # not been previously downloaded by another test
    if not DATA_DOMAIN_PATH.exists():
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)


def test_process_id_data():
    sae_df = process_id_data()

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 4
    es_test = sae_df[(sae_df.id_fi == "920000650") & (sae_df.id_an == 2019)]
    assert len(es_test) == 1
    assert es_test.iloc[0].id_rs == "HOPITAL FOCH"


def test_process_filtre_data():
    sae_df = process_id_data()

    sae_df = process_filtre_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 42
    es_test = sae_df[(sae_df.id_fi == "920000650") & (sae_df.id_an == 2019)]
    assert len(es_test) == 1
    assert es_test.iloc[0].filtre_heb_med


def test_process_mco_data():
    sae_df = process_id_data()

    sae_df = process_mco_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 32
    es_test = sae_df[(sae_df.id_fi == "920000650") & (sae_df.id_an == 2019)]
    assert len(es_test) == 1
    assert es_test.iloc[0].mco_lit_med == 267


def test_process_had_data():
    sae_df = process_id_data()

    sae_df = process_had_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 6
    es_test = sae_df[(sae_df.id_fi == "750806226") & (sae_df.id_an == 2019)]
    assert len(es_test) == 1
    assert es_test.iloc[0].had_platot == 803


def test_process_ssr_data():
    sae_df = process_id_data()

    sae_df = process_ssr_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 8
    es_test = sae_df[(sae_df.id_fi == "750000499") & (sae_df.id_an == 2019)]
    assert len(es_test) == 1
    assert es_test.iloc[0].ssr_lit == 10


def test_process_psy_data():
    sae_df = process_id_data()

    sae_df = process_psy_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 12
    es_test = sae_df[(sae_df.id_fi == "010000495") & (sae_df.id_an == 2019)]
    assert len(es_test) == 1
    assert es_test.iloc[0].psy_jou_htp_gen == 96916


def test_process_dialyse_data():
    sae_df = process_id_data()

    sae_df = process_dialyse_data(sae_df)

    assert len(sae_df) > 1e4
    assert sae_df.shape[1] == 34
    es_test = sae_df[(sae_df.id_fi == "920000650") & (sae_df.id_an == 2019)]
    assert len(es_test) == 1
    assert es_test.iloc[0].dialyse_capa_112ba == 12
