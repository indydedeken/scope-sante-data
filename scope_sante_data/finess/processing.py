import logging
from typing import Final

import pandas as pd

from scope_sante_data.constants import UTF_8
from scope_sante_data.data_acquisition_utils import clean_directory
from scope_sante_data.finess.constants import (
    FINAL_DOMAIN_PATH,
    PREPROCESSED_AUTORISATIONS_AS_PATH,
    PREPROCESSED_FINESS_ET_PATH,
)
from scope_sante_data.finess.processing_autorisations_as import (
    build_historized_autorisations_as_data,
)
from scope_sante_data.finess.processing_finess import (
    build_historized_finess_data,
)
from scope_sante_data.finess.processing_finess_ej import (
    build_historized_finess_ej_data,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_data():
    """
    Traite les données du domaine de données FINESS
    """
    logger.info("Processing data...")

    clean_directory_tree()

    finess_et = process_finess_et_data()
    finess_ej = process_finess_ej_data()
    finess_et = add_fields_from_ej(finess_et, finess_ej)
    finess_et.to_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    logger.debug("Historic finess ET data saved to disk")

    process_autorisations_as_data()

    logger.info("Processing data OK")


def clean_directory_tree():
    """
    Efface et reconstruit l'arborescence des dossiers
    """
    for path in [
        PREPROCESSED_FINESS_ET_PATH,
        PREPROCESSED_AUTORISATIONS_AS_PATH,
        FINAL_DOMAIN_PATH,
    ]:
        clean_directory(path)


def process_finess_et_data() -> pd.DataFrame:
    """
    Traite la source de données FINESS du domaine de données FINESS
    """
    historized_finess_df = build_historized_finess_data()
    logger.info(
        "%d FINESS ET have been historized among %d records",
        len(pd.unique(historized_finess_df["num_finess_et"])),
        len(historized_finess_df),
    )
    return historized_finess_df


def process_finess_ej_data() -> pd.DataFrame:
    """
    Traite la source de données FINESS EJ du domaine FINESS
    """
    historized_finessej_df = build_historized_finess_ej_data()
    logger.info(
        "%d FINESS EJ have been historized among %d records",
        len(pd.unique(historized_finessej_df["num_finess_ej"])),
        len(historized_finessej_df),
    )
    return historized_finessej_df


def process_autorisations_as_data():
    """
    Traite la source de données Autorisations AS (actes de soins) du domaine de
    données FINESS
    """
    historized_autorisations_as_data = build_historized_autorisations_as_data()
    logger.info(
        "%d autorisations AS have been historized among %d records",
        len(
            pd.unique(
                historized_autorisations_as_data[
                    [
                        "num_finess_et",
                        "num_autorisation",
                        "num_ligne_autorisee",
                        "num_mise_en_oeuvre",
                    ]
                ].values.ravel("K")
            )
        ),
        len(historized_autorisations_as_data),
    )

    historized_autorisations_as_data.to_csv(
        FINAL_DOMAIN_PATH / "autorisations_as.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    logger.debug("Historic finess AS saved to disk")


def add_fields_from_ej(
    finess_et: pd.DataFrame,
    finess_ej: pd.DataFrame,
) -> pd.DataFrame:
    """
    Cette fonction ajoute au référentiel des finess ET (géographiques) certains
    champs relatifs à l'EJ liée (exemple : statut juridique, raison sociale,...)
    """
    finess_et = pd.merge(
        left=finess_et,
        right=finess_ej[
            [
                "date_export",
                "num_finess_ej",
                "raison_sociale",
                "raison_sociale_longue",
                "statut_juridique",
                "libelle_statut_juridique",
            ]
        ],
        how="left",
        on=["num_finess_ej", "date_export"],
        suffixes=("_et", "_ej"),
    )
    # nous ne pouvons pas caster en "int64" car certains ET sont liés à des EJ
    # qui n'existent pas dans notre référentiel finess EJ !
    finess_et["statut_juridique"] = finess_et["statut_juridique"].astype(
        "Int64"
    )
    finess_et.rename(
        columns={
            "statut_juridique": "statut_juridique_ej",
            "libelle_statut_juridique": "libelle_statut_juridique_ej",
        },
        inplace=True,
    )
    # ajout de la valeur du statut juridique spécifique à Scope Santé
    # document de référence : http://finess.sante.gouv.fr/fininter/jsp/pdf.do?xsl=StatutJuridique.xsl
    finess_et.loc[
        finess_et["statut_juridique_ej"] <= 30, "statut_juridique"
    ] = "Public"
    finess_et.loc[
        (
            (finess_et["statut_juridique_ej"] >= 40)
            & (finess_et["statut_juridique_ej"] <= 66)
        )
        | (finess_et["statut_juridique_ej"] == 89),
        "statut_juridique",
    ] = "Privé à but non lucratif"
    finess_et.loc[
        (
            (finess_et["statut_juridique_ej"] >= 70)
            & (finess_et["statut_juridique_ej"] <= 88)
        )
        | (
            (finess_et["statut_juridique_ej"] >= 91)
            & (finess_et["statut_juridique_ej"] <= 95)
        ),
        "statut_juridique",
    ] = "Privé"

    # Champ type d'établissement
    # document de référence : http://finess.sante.gouv.fr/fininter/jsp/pdf.do?xsl=CategEta.xsl
    finess_et["type_etablissement"] = finess_et["statut_juridique"]
    finess_et.loc[
        finess_et["categorie_et"].isin([355, 106]), "type_etablissement"
    ] = "CH"
    finess_et.loc[
        finess_et["categorie_et"] == 131, "type_etablissement"
    ] = "CLCC"
    finess_et.loc[
        finess_et["raison_sociale_et"].str.contains("CHU "),
        "type_etablissement",
    ] = "CHU"

    return finess_et
