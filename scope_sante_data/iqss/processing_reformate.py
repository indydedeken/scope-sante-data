import logging
from typing import Final

import numpy as np
import pandas as pd

from scope_sante_data.iqss.constants import FINAL_IQSS_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


NON_APPLICABLE_MISSING_VALUE = "Non applicable"
NON_REPONDANT_MISSING_VALUE = "Non répondant"
NON_CONCERNE_MISSING_VALUE = "Non concerné"
NON_CONTROLE_MISSING_VALUE = "Non contrôlé"
NON_CALCULABLE_MISSING_VALUE = "Non calculable"
DONNEES_INSUFFISANTES_MISSING_VALUE = "Données insuffisantes"
MISSING_VALUES_DICT: Final[dict] = {
    "NA": NON_APPLICABLE_MISSING_VALUE,
    "Non Applicable": NON_APPLICABLE_MISSING_VALUE,
    "NR": NON_REPONDANT_MISSING_VALUE,
    "Non répondant": NON_REPONDANT_MISSING_VALUE,
    "NC": NON_CONCERNE_MISSING_VALUE,
    "Non concerné": NON_CONCERNE_MISSING_VALUE,
    "Non concerné (pas d'activité)": NON_CONCERNE_MISSING_VALUE,
    "Non concerné (non comparable)": NON_CONCERNE_MISSING_VALUE,
    "Non Contrôlable": NON_CONTROLE_MISSING_VALUE,
    "ES non contrôlé": NON_CONTROLE_MISSING_VALUE,
    "Pas de contrôle": NON_CONTROLE_MISSING_VALUE,
    "Non Calculable": NON_CALCULABLE_MISSING_VALUE,
    "DI": DONNEES_INSUFFISANTES_MISSING_VALUE,
}

SUPPORTED_DTYPES: Final[list] = [
    ("string", "value_string"),
    ("Int64", "value_integer"),
    ("Float64", "value_float"),
]


def reformate_historized_iqss():
    """
    Reformate les critères iqss en ligne
    """

    merged_df = pd.read_csv(
        FINAL_IQSS_PATH / "iqss.csv",
        dtype={
            "finess": "string",
        },
    )
    merged_df = merged_df.convert_dtypes()

    if not all(
        dtype in [dtype[0] for dtype in SUPPORTED_DTYPES]
        for dtype in merged_df.dtypes.unique()
    ):
        logger.error(
            "Unsupported dtype detected : some variables will be lost in key-value format"
        )

    typed_dfs = []
    for dtype, column_name in SUPPORTED_DTYPES:
        typed_df = create_key_value_typed_df(merged_df, dtype, column_name)
        typed_dfs.append(typed_df)
    reformate_df = pd.concat(typed_dfs)
    reformate_df = extract_missing_values(reformate_df)
    reformate_df = clean_string_values(reformate_df)

    return reformate_df


def create_key_value_typed_df(
    iqss_df: pd.DataFrame,
    dtype: str,
    column_name: str,
) -> pd.DataFrame:
    """
    Create and return a key-value typed dataframe (a dataframe with all values
    of a common type in the same column) according to the given parameters.
    """
    id_columns = ["finess", "finess_type", "raison_sociale", "annee"]
    typed_columns = iqss_df.select_dtypes(dtype).columns
    typed_columns = typed_columns.difference(id_columns).tolist()
    typed_df = iqss_df[id_columns + typed_columns]
    typed_df = typed_df.melt(
        id_vars=id_columns,
        value_vars=typed_columns,
        var_name="key",
        value_name=column_name,
    )
    typed_df.dropna(subset=[column_name], inplace=True)
    return typed_df


def extract_missing_values(iqss_df: pd.DataFrame) -> pd.DataFrame:
    """
    Extrait et harmonise les valeurs non-nulles identifiées comme valeurs
    manquantes dans une colonne dédiée nommée "missing_value"
    """
    iqss_df["missing_value"] = iqss_df["value_string"]
    iqss_missing_values = iqss_df["missing_value"].isin(
        MISSING_VALUES_DICT.keys()
    )
    iqss_df.loc[iqss_missing_values, "value_string"] = np.NaN
    iqss_df.loc[~iqss_missing_values, "missing_value"] = np.NaN
    iqss_df["missing_value"].replace(MISSING_VALUES_DICT, inplace=True)
    return iqss_df


def clean_string_values(iqss_df: pd.DataFrame) -> pd.DataFrame:
    """
    Nettoie la colonne value_string en séparant les valeurs interprétables
    numériquement des valeurs purement textuelles. Supprime également les
    valeurs textuelles aberrantes.
    """
    # traitement des pourcentages
    percents_mask = iqss_df["value_string"].str.endswith("%", na=False)
    iqss_df.loc[percents_mask, "value_float"] = (
        iqss_df.loc[percents_mask, "value_string"]
        .str.rstrip("%")
        .astype("float")
        / 100
    )
    iqss_df.loc[percents_mask, "value_string"] = np.NaN

    # suppression des valeurs ne contenant qu'un point
    dot_mask = iqss_df["value_string"] == "."
    logger.info(
        "Cleaning dot values (.) from the following keys : %s",
        iqss_df[dot_mask]["key"].unique().tolist(),
    )
    iqss_df = iqss_df.drop(iqss_df[dot_mask].index)

    # casting du reste des données
    numerics = pd.to_numeric(
        iqss_df["value_string"],
        errors="coerce",
    )
    numeric_mask = numerics.notna()
    iqss_df.loc[numeric_mask, "value_string"] = np.NaN
    iqss_df.loc[numeric_mask, "value_float"] = numerics[numeric_mask]
    return iqss_df
