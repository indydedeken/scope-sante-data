import logging
from typing import Final

import pandas as pd

from scope_sante_data.data_acquisition_utils import clean_directory
from scope_sante_data.iqss.constants import (
    CLEAN_IQSS_PATH,
    FINAL_IQSS_PATH,
    RESOURCES_IQSS_PATH,
)
from scope_sante_data.iqss.processing_clean import clean_data
from scope_sante_data.iqss.processing_finess import set_finess_type
from scope_sante_data.iqss.processing_merge import merged_iqss_data
from scope_sante_data.iqss.processing_reformate import (
    reformate_historized_iqss,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_data():
    """
    Traite les données du domaine IQSS
    """
    logger.info("Processing data...")
    for directory in [CLEAN_IQSS_PATH, FINAL_IQSS_PATH]:
        clean_directory(directory)

    metadata_df = pd.read_csv(RESOURCES_IQSS_PATH / "metadata.csv", sep=",")

    clean_data(metadata_df)
    logger.info("cleaned directory")

    merged_iqss_data()
    logger.info("merged data")

    set_finess_type()
    logger.info("add finess type OK")

    reformate_df = reformate_historized_iqss()
    logger.info("reformated data")

    reformate_df.to_csv(
        FINAL_IQSS_PATH / "iqss_key_value.csv",
        index=False,
        sep=",",
        encoding="UTF-8",
    )
    logger.info("processed data")
