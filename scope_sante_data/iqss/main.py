import logging
from typing import Final

from scope_sante_data import data_acquisition_utils
from scope_sante_data.iqss.constants import (
    FINAL_IQSS_PATH,
    RESOURCES_IQSS_PATH,
    SCHEMAS_IQSS_PATH,
)
from scope_sante_data.iqss.data_acquisition import download_data
from scope_sante_data.iqss.processing import process_data
from scope_sante_data.iqss.validate_metadata import validate_metadata_file
from scope_sante_data.validation_utils import is_data_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(skip_data_download: bool = False, data_validation: bool = False):
    """
    Point d'entrée de tout le pipeline iqss
    """
    # acquisition des données
    if skip_data_download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_IQSS_PATH
        )
        download_data(data_sources)

    # validation du fichier de metadonnées
    validate_metadata_file()

    # process des données
    process_data()

    # validation de la donnée
    if data_validation:
        logger.info("Validating data...")
        valid = is_data_valid(
            FINAL_IQSS_PATH / "iqss_key_value.csv",
            SCHEMAS_IQSS_PATH / "iqss_key_value.json",
        )
        logger.info("Validating data : %s", "VALID" if valid else "INVALID")
    else:
        logger.info("Skipped data validation")
