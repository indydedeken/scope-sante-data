import logging
from typing import Final

from scope_sante_data import data_acquisition_utils
from scope_sante_data.certification.constants import (
    FINAL_DOMAIN_PATH,
    RESOURCES_DOMAIN_PATH,
    SCHEMAS_DOMAIN_PATH,
)
from scope_sante_data.certification.data_download import download_data
from scope_sante_data.certification.processing import process_data
from scope_sante_data.validation_utils import is_data_valid

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(skip_data_download: bool = False, data_validation: bool = False):
    """
    Point d'entrée du pipeline de données de certifications
    """
    # download data
    if skip_data_download:
        logger.info("Skipped data download")
    else:
        data_sources = data_acquisition_utils.load_data_sources_file(
            RESOURCES_DOMAIN_PATH
        )
        download_data(data_sources)

    process_data()

    # data validation
    if data_validation:
        logger.info("Validating data...")
        for filename in ["certifications", "thematiques"]:
            valid = is_data_valid(
                FINAL_DOMAIN_PATH / f"{filename}.csv",
                SCHEMAS_DOMAIN_PATH / f"{filename}.json",
            )
            logger.info(
                "Validating %s data : %s",
                filename,
                "VALID" if valid else "INVALID",
            )
    else:
        logger.info("Skipped data validation")
