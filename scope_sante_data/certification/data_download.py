import logging
import os
import shutil
from typing import Final

import requests

from scope_sante_data.certification.constants import RAW_DOMAIN_PATH

logger: Final[logging.Logger] = logging.getLogger(__name__)


def download_data(data_sources):
    """
    Télécharge les données de certifications
    """
    logger.info("Downloading data...")

    # clean directory structure
    shutil.rmtree(RAW_DOMAIN_PATH, ignore_errors=True)
    os.makedirs(RAW_DOMAIN_PATH)

    data_source = data_sources["sources"]
    # download data
    with requests.get(data_source["certifications"]) as response, open(
        RAW_DOMAIN_PATH / "certifications.csv", "wb"
    ) as file:
        file.write(response.content)
    # download thematiques
    with requests.get(data_source["thematiques"]) as response, open(
        RAW_DOMAIN_PATH / "thematiques.csv", "wb"
    ) as file:
        file.write(response.content)
    # download thematiques labels
    with requests.get(data_source["thematiques_labels"]) as response, open(
        RAW_DOMAIN_PATH / "thematiques_labels.csv", "wb"
    ) as file:
        file.write(response.content)

    logger.info("Downloading data OK")
