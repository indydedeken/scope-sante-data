from pathlib import Path
from typing import Final

from scope_sante_data.constants import DATA_PATH, RESOURCES_PATH, SCHEMAS_PATH

RESOURCES_DOMAIN_PATH: Final[Path] = RESOURCES_PATH / "certification"
SCHEMAS_DOMAIN_PATH: Final[Path] = SCHEMAS_PATH / "certification"
DATA_DOMAIN_PATH: Final[Path] = DATA_PATH / "certification"
RAW_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "raw"
FINAL_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "final"

CERTIF_V2014_DECISION_THEMATIQUE_KEY_PREFIX: Final[
    str
] = "certif_V2014_decision_thematique_"
