import datetime
from datetime import date

import numpy as np
import pandas as pd

from scope_sante_data.scope_sante import data_reader


def get_scope_finess(
    valeurs_df: pd.DataFrame,
) -> pd.DataFrame:
    """
    Règles de gestion pour la prise en compte de l’évolution des Finess au cours
    du temps :
        - Les Finess fermés en année N-2 sont supprimés du référentiel Scope santé
        - Les Finess fermés en année N-1, N et N+1 sont présents dans le
        référentiel Scope santé (la fiche détaillée de l’établissement reste
        disponible en pied de page du site dans la rubrique « Toutes les fiches
        établissements ») mais ne sont plus géoréférencés sur la carte de France
    """
    finess_df = data_reader.get_finess()
    autorisations_as = get_scope_autorisations_as()
    # le filtre par autorisations AS seul ne permettant pas d'inclure tous les
    # établissements pour lesquels nous avons des indicateurs (tous domaines
    # confondus), il a été élargi pour inclure tous ces établissements
    finess_df = finess_df[
        finess_df["num_finess_et"].isin(autorisations_as["num_finess_et"])
        | finess_df["num_finess_et"].isin(valeurs_df["finess"])
        | finess_df["num_finess_ej"].isin(valeurs_df["finess"])
    ].copy()

    year_n = pd.to_datetime(max(finess_df["date_export"].unique())).year

    # ajout du flag actif_scope_sante indiquant les établissements concernés
    # par Scope Santé cette année
    finess_df["actif_scope_sante"] = False
    finess_df.loc[
        (  # finess de l'année N (ouverts et fermés)
            (finess_df["date_export"] > f"{year_n}-01-01")
            | (  # finess fermés en année N-1
                (f"{year_n - 1}-01-01" < finess_df["date_export"])
                & (finess_df["date_export"] < f"{year_n}-01-01")
                & (finess_df["ferme_cette_annee"])
            )
        ),
        "actif_scope_sante",
    ] = True

    # ajout du flag dernier_enregistrement indiquant qu'une ligne de donnée est
    # la plus récente pour un établissement donné
    finess_df["dernier_enregistrement"] = False
    finess_df["temp_key"] = finess_df["num_finess_et"].astype(str) + finess_df[
        "date_export"
    ].astype(str)
    latest_finess_df = finess_df.groupby(
        ["num_finess_et"], as_index=False
    ).agg({"date_export": np.max})
    latest_finess_df["temp_key"] = latest_finess_df["num_finess_et"].astype(
        str
    ) + latest_finess_df["date_export"].astype(str)
    finess_df.loc[
        finess_df["temp_key"].isin(latest_finess_df["temp_key"]),
        "dernier_enregistrement",
    ] = True
    finess_df.drop(columns="temp_key", inplace=True)

    return finess_df


def get_scope_autorisations_as() -> pd.DataFrame:
    """
    Les établissements disposant des autorisations d’activités de soins
    suivantes sont affichés dans Scope Santé : Médecine, Chirurgie,
    Gynécologie/Obstétrique, Psychiatrie (en hospitalisation complète),
    Soins de longue durée, Soins de suite et de réadaptation et
    Hospitalisation à domicile.
    """
    # first we filter on active autorisations
    autorisations_as = data_reader.get_autorisations_as()
    autorisations_as = autorisations_as[
        autorisations_as["date_fin"]
        > datetime.datetime(year=date.today().year, month=1, day=1)
    ]

    # then we filter by activite
    autorisations_as = autorisations_as[
        autorisations_as["activite"].isin(
            [
                1,  # Médecine
                2,  # Chirurgie
                3,  # Gynécologie/obstétrique
                4,  # Psychiatrie
                7,  # Soins de longue durée
                *range(50, 60),  # SSR spécialisés et non spécialisés
            ]
        )
        | (autorisations_as["forme"] == 5)  # Hospitalisation à domicile
    ]
    # Psychiatrie en hospitalisation complète uniquement (forme = 1)
    autorisations_as = autorisations_as.drop(
        autorisations_as[
            (autorisations_as["activite"] == 4)
            & (autorisations_as["forme"] != 1)
        ].index
    )

    return autorisations_as
