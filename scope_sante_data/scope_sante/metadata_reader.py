import pandas as pd

from scope_sante_data.certification import constants as certification_constants
from scope_sante_data.constants import UTF_8
from scope_sante_data.esatis import constants as esatis_constants
from scope_sante_data.iqss import constants as iqss_constants
from scope_sante_data.sae import constants as sae_constants
from scope_sante_data.scope_sante.constants import NOMENCLATURES_DTYPES_DICT


def get_sae_metadata() -> pd.DataFrame:
    sae_metadata = pd.read_csv(
        sae_constants.FINAL_DOMAIN_PATH / "sae_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    sae_metadata["source"] = "SAE"
    return sae_metadata


def get_certification_metadata() -> pd.DataFrame:
    certification_metadata = pd.read_csv(
        certification_constants.FINAL_DOMAIN_PATH
        / "certification_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    certification_metadata["source"] = "Certification"
    return certification_metadata


def get_certification_nomenclatures() -> pd.DataFrame:
    nomenclatures = pd.read_csv(
        certification_constants.RESOURCES_DOMAIN_PATH / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        dtype=NOMENCLATURES_DTYPES_DICT,
    )
    nomenclatures["source"] = "Certification"
    return nomenclatures


def get_iqss_metadata() -> pd.DataFrame:
    iqss_metadata = pd.read_csv(
        iqss_constants.RESOURCES_IQSS_PATH / "metadata.csv",
        sep=",",
        encoding=UTF_8,
        dtype={
            "version": "Int64",
            "annee": "Int64",
        },
    )
    iqss_metadata["code"] = iqss_metadata["code"].str.lower()
    iqss_metadata.rename(
        columns={
            "code": "name",
            "libelle": "title",
            "commentaire": "description",
        },
        inplace=True,
    )
    return iqss_metadata


def get_iqss_nomenclatures() -> pd.DataFrame:
    nomenclatures = pd.read_csv(
        iqss_constants.RESOURCES_IQSS_PATH / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        dtype=NOMENCLATURES_DTYPES_DICT,
    )
    return nomenclatures


def get_esatis_metadata() -> pd.DataFrame:
    metadata = pd.read_csv(
        esatis_constants.FINAL_ESATIS_PATH / "esatis_metadata.csv",
        sep=",",
        encoding=UTF_8,
    )
    metadata["source"] = "IQSS questionnaire patient"
    return metadata


def get_esatis_nomenclatures() -> pd.DataFrame:
    nomenclatures = pd.read_csv(
        esatis_constants.RESOURCES_ESATIS_PATH / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        dtype=NOMENCLATURES_DTYPES_DICT,
    )
    nomenclatures["source"] = "IQSS questionnaire patient"
    return nomenclatures
