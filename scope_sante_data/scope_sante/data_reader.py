from functools import lru_cache

import pandas as pd

from scope_sante_data.certification import constants as certification_constants
from scope_sante_data.certification.processing import (
    CERTIFICATIONS_DTYPES_DICT,
    THEMATIQUES_DTYPES_DICT,
)
from scope_sante_data.constants import UTF_8
from scope_sante_data.esatis import constants as esatis_constants
from scope_sante_data.finess import constants as finess_constants
from scope_sante_data.finess.processing_autorisations_as import AS_DTYPES_DICT
from scope_sante_data.finess.processing_finess import FINAL_FINESS_DTYPES_DICT
from scope_sante_data.iqss import constants as iqss_constants
from scope_sante_data.sae import constants as sae_constants
from scope_sante_data.scope_sante.constants import KEY_VALUE_DTYPES_DICT


@lru_cache
def get_finess() -> pd.DataFrame:
    finess = pd.read_csv(
        finess_constants.FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        dtype=FINAL_FINESS_DTYPES_DICT,
        parse_dates=["date_export"],
    )
    return finess


@lru_cache
def get_autorisations_as() -> pd.DataFrame:
    autorisations_as = pd.read_csv(
        finess_constants.FINAL_DOMAIN_PATH / "autorisations_as.csv",
        sep=",",
        encoding=UTF_8,
        dtype=AS_DTYPES_DICT,
        parse_dates=["date_export", "date_fin"],
        date_parser=pd.to_datetime,
    )
    return autorisations_as


@lru_cache
def get_certifications() -> pd.DataFrame:
    certifications = pd.read_csv(
        certification_constants.FINAL_DOMAIN_PATH / "certifications.csv",
        sep=",",
        encoding=UTF_8,
        dtype=CERTIFICATIONS_DTYPES_DICT,
    )
    return certifications


@lru_cache
def get_certifications_kv() -> pd.DataFrame:
    certifications_kv = pd.read_csv(
        certification_constants.FINAL_DOMAIN_PATH
        / "certifications_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
        parse_dates=["value_date"],
    )
    return certifications_kv


@lru_cache
def get_thematiques() -> pd.DataFrame:
    thematiques = pd.read_csv(
        certification_constants.FINAL_DOMAIN_PATH / "thematiques.csv",
        sep=",",
        encoding=UTF_8,
        dtype=THEMATIQUES_DTYPES_DICT,
    )
    return thematiques


@lru_cache
def get_sae() -> pd.DataFrame:
    sae = pd.read_csv(
        sae_constants.FINAL_DOMAIN_PATH / "sae_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )
    return sae


@lru_cache
def get_iqss() -> pd.DataFrame:
    iqss = pd.read_csv(
        iqss_constants.FINAL_IQSS_PATH / "iqss_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )
    return iqss


@lru_cache
def get_esatis() -> pd.DataFrame:
    esatis = pd.read_csv(
        esatis_constants.FINAL_ESATIS_PATH / "esatis_key_value.csv",
        sep=",",
        encoding=UTF_8,
        dtype=KEY_VALUE_DTYPES_DICT,
    )
    return esatis
