import logging
from typing import Final

from scope_sante_data.constants import FINESS_TYPE_UNKNOWN, UTF_8
from scope_sante_data.data_acquisition_utils import clean_directory
from scope_sante_data.scope_sante import data_reader
from scope_sante_data.scope_sante.constants import FINAL_DOMAIN_PATH
from scope_sante_data.scope_sante.processing_finess import get_scope_finess
from scope_sante_data.scope_sante.processing_metadata import (
    build_metadata,
    build_nomenclatures,
)
from scope_sante_data.scope_sante.processing_valeurs import (
    build_key_value_table,
    set_finess_type,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def process_data():
    """Aggrège les données des différents domaines"""
    logger.info("Processing data...")

    clean_directory(FINAL_DOMAIN_PATH)

    valeurs_df = build_key_value_table()
    finess_df = get_scope_finess(valeurs_df)
    valeurs_df = set_finess_type(valeurs_df, finess_df)

    # clean unknown finess
    unknown_finess_index = valeurs_df["finess_type"] == FINESS_TYPE_UNKNOWN
    logger.info(
        "Les finess suivants ne sont pas référencés et vont être nettoyés : %s",
        valeurs_df[unknown_finess_index]["finess"].unique().to_numpy(),
    )
    valeurs_df = valeurs_df[~unknown_finess_index]

    metadata_df = build_metadata(valeurs_df)
    nomenclatures_df = build_nomenclatures()

    # save data to disk
    finess_df.to_csv(
        FINAL_DOMAIN_PATH / "finess.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    data_reader.get_autorisations_as().to_csv(
        FINAL_DOMAIN_PATH / "autorisations_as.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    valeurs_df.to_csv(
        FINAL_DOMAIN_PATH / "valeurs.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    metadata_df.to_csv(
        FINAL_DOMAIN_PATH / "metadata.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    metadata_df.to_excel(
        FINAL_DOMAIN_PATH / "metadata.xlsx",
        encoding=UTF_8,
        index=False,
    )
    nomenclatures_df.to_csv(
        FINAL_DOMAIN_PATH / "nomenclatures.csv",
        sep=",",
        encoding=UTF_8,
        index=False,
    )
    nomenclatures_df.to_excel(
        FINAL_DOMAIN_PATH / "nomenclatures.xlsx",
        encoding=UTF_8,
        index=False,
    )

    logger.info("Processing data OK")
