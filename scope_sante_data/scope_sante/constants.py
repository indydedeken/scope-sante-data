from pathlib import Path
from typing import Final

from scope_sante_data.constants import DATA_PATH, RESOURCES_PATH, SCHEMAS_PATH

RESOURCES_DOMAIN_PATH: Final[Path] = RESOURCES_PATH / "scope_sante"
DATA_DOMAIN_PATH: Final[Path] = DATA_PATH / "scope_sante"
FINAL_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "final"
SCHEMAS_DOMAIN_PATH: Final[Path] = SCHEMAS_PATH / "scope_sante"

KEY_VALUE_DTYPES_DICT: Final[dict] = {
    "annee": "int64",
    "finess": "string",
    "finess_type": "string",
    "key": "string",
    "value_boolean": "boolean",
    "value_string": "string",
    "value_integer": "Int64",
    "value_float": "Float64",
    "missing_value": "string",
}

NOMENCLATURES_DTYPES_DICT: Final[dict] = {
    "key": "string",
    "value": "string",
    "label": "string",
}

# Thématiques de certification : la correspondance entre numéros et thématiques
# associés est extraite du fichier "libellés des thématiques"
# (thematiques_labels.csv dans data/certification/raw)
CERTIF_THEMATIQUE_URGENCES: Final[list] = [14, 72]
CERTIF_THEMATIQUE_BLOC: Final[list] = [15, 47, 73, 133]
CERTIF_THEMATIQUE_RADIOTHERAPIE: Final[list] = [16, 74]
CERTIF_THEMATIQUE_IMAG_INTERV: Final[list] = [18, 76]
CERTIF_THEMATIQUE_BIOLOGIE: Final[list] = [12, 70]
CERTIF_THEMATIQUE_IMAGERIE: Final[list] = [13, 71]
CERTIF_THEMATIQUE_ENDOSCOPIE: Final[list] = [19, 77]
CERTIF_THEMATIQUE_NAISSANCE: Final[list] = [20, 78]
CERTIF_THEMATIQUE_FIN_DE_VIE: Final[list] = [8, 66]
CERTIF_THEMATIQUE_ID_PATIENT: Final[list] = [10, 40, 68, 135]
CERTIF_THEMATIQUE_DON_ORGANE: Final[list] = [21, 79]
