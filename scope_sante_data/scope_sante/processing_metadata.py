import pandas as pd

from scope_sante_data.constants import FINESS_TYPE_MIXTE, FINESS_TYPE_UNKNOWN
from scope_sante_data.scope_sante import metadata_reader


def build_metadata(valeurs_df: pd.DataFrame) -> pd.DataFrame:
    """
    Cette fonction construit et retourne le dataframe de métadonnées "final" i.e
    résultant de l'agrégation des métadonnées des autres domaines. Les
    métadonnées sont également enrichies à ce niveau.
    """
    metadata = pd.concat(
        [
            metadata_reader.get_certification_metadata(),
            metadata_reader.get_iqss_metadata(),
            metadata_reader.get_esatis_metadata(),
            metadata_reader.get_sae_metadata(),
        ]
    )

    # cleaning
    metadata.drop_duplicates(inplace=True)
    required_keys = valeurs_df["key"].unique()
    metadata = metadata[metadata["name"].isin(required_keys)]

    metadata = add_finess_type(metadata, valeurs_df)

    return metadata


def add_finess_type(metadata, valeurs_df) -> pd.DataFrame:
    """
    Cette fonction ajoute la colonne "finess_type" au fichier de métadonnées
    en fonction des types des finess déjà réconciliés dans le fichier valeurs
    """
    metadata["finess_type"] = metadata["name"].map(
        lambda name: get_finess_type(name, valeurs_df)
    )
    return metadata


def get_finess_type(key, valeurs_df) -> str:
    finess_types = valeurs_df[valeurs_df["key"] == key]["finess_type"]
    finess_types = finess_types[finess_types != FINESS_TYPE_UNKNOWN]
    return (
        FINESS_TYPE_MIXTE
        if finess_types.nunique() > 1
        else finess_types.iloc[0]
    )


def build_nomenclatures() -> pd.DataFrame:
    """
    Construit et retourne le dataframe agrégeant les tables de nomenclatures
    de chaque domaine de données
    """
    nomenclatures = pd.concat(
        [
            metadata_reader.get_certification_nomenclatures(),
            metadata_reader.get_esatis_nomenclatures(),
            metadata_reader.get_iqss_nomenclatures(),
        ]
    )
    return nomenclatures
