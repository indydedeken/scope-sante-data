import logging
from typing import Final

from requests import (  # pylint: disable=redefined-builtin
    ConnectionError,
    HTTPError,
)

from scope_sante_data import data_acquisition_utils
from scope_sante_data.certification import (
    constants as certification_constants,
    main as certification_main,
)
from scope_sante_data.constants import DATA_GOUV_API_KEY
from scope_sante_data.esatis import (
    constants as esatis_constants,
    main as esatis_main,
)
from scope_sante_data.finess import (
    constants as finess_constants,
    main as finess_main,
)
from scope_sante_data.iqss import (
    constants as iqss_constants,
    main as iqss_main,
)
from scope_sante_data.sae import constants as sae_constants, main as sae_main
from scope_sante_data.scope_sante.constants import RESOURCES_DOMAIN_PATH
from scope_sante_data.scope_sante.open_data import (
    update_metadata,
    upload_open_data,
)
from scope_sante_data.scope_sante.processing import process_data

logger: Final[logging.Logger] = logging.getLogger(__name__)


def main(
    force_full_run: bool,
    data_validation: bool,
    open_data: bool,
):
    """
    Point d'entrée du pipeline Scope Santé. Ce pipeline agrège les sorties des
    pipelines des autres domaines de données constitutifs des données Scope Santé
    """
    if force_full_run or not finess_constants.FINAL_DOMAIN_PATH.exists():
        logger.info("Launching the FINESS pipeline")
        finess_main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if (
        force_full_run
        or not certification_constants.FINAL_DOMAIN_PATH.exists()
    ):
        logger.info("Launching the certification pipeline")
        certification_main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if force_full_run or not sae_constants.FINAL_DOMAIN_PATH.exists():
        logger.info("Launching the SAE pipeline")
        sae_main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if force_full_run or not iqss_constants.FINAL_IQSS_PATH.exists():
        logger.info("Launching the IQSS pipeline")
        iqss_main.main(
            skip_data_download=False, data_validation=data_validation
        )
    if force_full_run or not esatis_constants.FINAL_ESATIS_PATH.exists():
        logger.info("Launching the e-Satis pipeline")
        esatis_main.main(
            skip_data_download=False, data_validation=data_validation
        )

    process_data()

    if open_data:
        if DATA_GOUV_API_KEY is None:
            logger.warning(
                "Missing API key for data.gouv.fr, skipping upload in open data"
            )
        else:
            data_sources = data_acquisition_utils.load_data_sources_file(
                RESOURCES_DOMAIN_PATH
            )
            try:
                update_metadata(data_sources)
            except (ConnectionError, HTTPError) as error:
                raise RuntimeError(
                    "Unable to update metadata on data.gouv.fr, skipping data upload"
                ) from error

            try:
                upload_open_data(data_sources)
            except (ConnectionError, HTTPError) as error:
                raise RuntimeError("Datagouv upload failed") from error
