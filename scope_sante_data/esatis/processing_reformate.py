import logging
from typing import Final

import pandas as pd

from scope_sante_data.esatis.constants import FINAL_ESATIS_PATH
from scope_sante_data.scope_sante.reformate_utils import (
    create_float_df,
    create_object_df,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def reformate_historized():
    """
    Merge les fichiers et sauvegarde l'historisation d'e-satis
    """

    historized_esatis = pd.read_csv(FINAL_ESATIS_PATH / "esatis.csv")

    historized_esatis[["finess_geo", "finess_ej", "finess_pmsi"]].astype(
        dtype=str
    )
    if not all(
        isinstance(dtype, (float, object))
        for dtype in historized_esatis.dtypes.unique()
    ):
        logger.warning(
            "reformate_historized_esatis() support only object and float type"
        )

    list_idx = ["finess_geo", "finess_ej", "finess_pmsi", "annee"]
    df_str = create_object_df(historized_esatis, list_idx)
    df_float = create_float_df(historized_esatis, list_idx)
    reformate_df = pd.merge(
        df_float,
        df_str,
        how="outer",
        on=["finess_geo", "finess_ej", "finess_pmsi", "annee", "key"],
        suffixes=("", "_y"),
    )
    reformate_df.drop_duplicates(inplace=True)
    if not reformate_df["finess_geo"].isnull().any():
        reformate_df["finess_type"] = "geo"
        reformate_df.rename(columns={"finess_geo": "finess"}, inplace=True)
    else:
        logger.error("Il existe des finess géographique à null")
    reformate_df.drop(
        columns=["finess_ej", "finess_pmsi"], inplace=True, errors="ignore"
    )
    return reformate_df
