import logging
from typing import Final

import numpy as np

from scope_sante_data.data_acquisition_utils import read_file
from scope_sante_data.esatis.constants import (
    CLEAN_ESATIS_PATH,
    RAW_ESATIS_PATH,
)

logger: Final[logging.Logger] = logging.getLogger(__name__)


def clean_columns(esatis_df, file_path, source_name):
    """
    Nettoie les colonnes des fichiers
    """

    columns_same_name = [
        "taux_reco_brut",
        "nb_reco_brut",
        "classement",
        "evolution",
        "score_all_ajust",
    ]
    colums_2_drop = [
        "participation",
        "depot",
        "rs_finess",
        "rs_finess_geo",
        "region",
        "type",
        "region_id",
        "",
    ]
    ext = "_" + file_path.name.split("_")[1].split(".")[0]

    esatis_df.columns = [critere.lower() for critere in esatis_df.columns]
    # drop useless columns
    esatis_df.drop(columns=colums_2_drop, inplace=True, errors="ignore")
    # add year data
    esatis_df["annee"] = int(source_name.name.split("_")[-1])
    # rename columns names and set good type
    esatis_df.rename(
        columns={"finess": "finess_ej", "finess_plage": "finess_pmsi"},
        inplace=True,
        errors="ignore",
    )

    for column_name in columns_same_name:
        if ext in ["_ca", "_48h"]:
            esatis_df = esatis_df.rename(
                columns={column_name: column_name + ext}, errors="ignore"
            )
        else:
            logger.warning(
                "Le fichier n'appartient ni au domaine CA ni au domaine + de 48h"
            )
    return esatis_df


def clean_values(esatis_df):
    """
    Nettoie les colonnes des fichiers
    """
    columns_string_list = [
        "classement_48h",
        "classement_ca",
        "evolution_48h",
        "evolution_ca",
        "finess_ej",
        "finess_geo",
        "finess_pmsi",
    ]
    columns_float_list = [
        "nb_rep_score_all_rea_ajust",
        "score_acc_ajust",
        "score_accueil_rea_ajust",
        "score_all_ajust_ca",
        "score_all_ajust_48h",
        "score_all_rea_ajust",
        "score_avh_ajust",
        "score_cer_ajust",
        "score_chambre_rea_ajust",
        "score_ovs_ajust",
        "score_pec_ajust",
        "score_pecinf_rea_ajust",
        "score_pecmed_rea_ajust",
        "score_repas_rea_ajust",
        "score_sortie_rea_ajust",
        "taux_reco_brut_48h",
        "taux_reco_brut_ca",
    ]

    columns_2_string = [
        column for column in columns_string_list if column in esatis_df.columns
    ]
    columns_2_float = [
        column for column in columns_float_list if column in esatis_df.columns
    ]
    if columns_2_string:
        esatis_df[columns_2_string] = esatis_df[columns_2_string].astype(str)
    if columns_2_float:
        esatis_df[columns_2_float] = esatis_df[columns_2_float].replace(
            [".", ","], [np.nan, "."], regex=True
        )
        esatis_df[columns_2_float].astype(float)
    return esatis_df


def clean_data():
    """
    Nettoyer les fichiers esatis
    """

    for year_data_path in sorted(RAW_ESATIS_PATH.iterdir()):
        files = [
            file_path
            for file_path in sorted(year_data_path.iterdir())
            if (year_data_path / file_path).is_file()
        ]
        for file_path in files:
            source_name = CLEAN_ESATIS_PATH / year_data_path.name
            # créer un sous dossier dans clean si il n'existe pas
            if not source_name.exists():
                source_name.mkdir(parents=True)
            # read file
            esatis_df = read_file(file_path)
            # clean file
            esatis_df = clean_columns(esatis_df, file_path, source_name)
            esatis_df = clean_values(esatis_df)
            # save file
            name = file_path.name.split(".")[0] + ".csv"
            esatis_df.to_csv(
                source_name / name, sep=",", index=False, encoding="UTF-8"
            )
