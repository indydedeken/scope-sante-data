import logging
from typing import Final

from frictionless import Resource, validate, validate_schema

logger: Final[logging.Logger] = logging.getLogger(__name__)


def is_metadata_valid(schema) -> bool:
    """
    Validate the given metadata file, typically a table schema.
    Return True if the metadata are valid, False otherwise
    """
    report = validate_schema(schema)
    return report.valid


def is_data_valid(data_path, metadata_path) -> bool:
    """
    Validate the data at the given path against the metadata (usually a table
    schema) at the given path.
    Return True if the data are valid regarding the metadata, False otherwise
    """
    resource = Resource(data_path, schema=metadata_path)
    report = validate(resource, type="resource", limit_memory=None)
    for error in report.task.errors:
        logger.warning(error.message)
    return report.valid
