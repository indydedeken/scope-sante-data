from pathlib import Path
from typing import Final

from scope_sante_data.constants import DATA_PATH, RESOURCES_PATH, SCHEMAS_PATH

RESOURCES_DOMAIN_PATH: Final[Path] = RESOURCES_PATH / "sae"
SCHEMAS_DOMAIN_PATH: Final[Path] = SCHEMAS_PATH / "sae"
DATA_DOMAIN_PATH: Final[Path] = DATA_PATH / "sae"
RAW_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "raw"
FINAL_DOMAIN_PATH: Final[Path] = DATA_DOMAIN_PATH / "final"

METADATA_SECTEUR_DICT: Final[dict] = {
    "mco": "MCO",
    "blocs": "MCO",
    "perinat": "MCO",
    "had": "HAD",
    "ssr": "SSR",
    "psy": "PSY",
    "dialyse": "MCO",
}
