# Motivations pour la création du jeu de données

La base Scope Santé regroupe les informations sur la qualité des hôpitaux et des cliniques produites par la Haute Autorité de Santé, à savoir :
- les résultats de certification,
- les indicateurs de qualité et de sécurité des soins associés.

Il contient également différentes informations de contexte, caractérisant les établissements de santé :
- les informations d'immatriculation au répertoire FINESS,
- les activités,
- les capacités.

Ce jeu de données alimente un nouvel espace dédié à la qualité des hôpitaux et cliniques sur le site internet de la HAS,
accessible via [ce lien](https://www.has-sante.fr/jcms/c_1725555/fr/qualite-des-hopitaux-et-des-cliniques) après son lancement en 2022,
en remplacement du site internet de Scope Santé.

Il est destiné à tout acteur du secteur de la santé ou tous les citoyens qui souhaiteraient réutiliser ces données pour d'autres usages.

Ce jeu de données est financé et soutenu par la Haute Autorité de la Santé.

# Composition du jeu de données

Cette section présente les origines des données, la structure de la base Scope Santé et les dépendances tierces.

## Origines des données

La base Scope Santé résulte du traitement et du regroupement de cinq sources de données, toutes disponibles en open data :


1.	Les données de certification

Mises à disposition par la HAS, elles regroupent les résultats des procédures de certifications suivies par certains établissements de santé (voir [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/certification.html) pour plus d’information).

2.	Les données IQSS

Mises à disposition par la HAS, elles contiennent les valeurs des différents Indicateurs de Qualité et de Sécurité des Soins associés aux établissements (voir [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/iqss.html) pour plus d’information).

3.	Les données e-Satis

Mises à disposition par la HAS, elles contiennent des mesures de la satisfaction des patients pour chacun des établissements (voir [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/esatis.html) pour plus d’information).

4.	Les données FINESS

Mises à disposition par le Ministère des Solidarités et de la Santé, elles regroupent tous les établissements disposant d’une autorisation d’activité ou d’un agrément, ainsi que des informations caractérisant ces derniers (voir [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/finess.html) pour plus d’information).


5.	Les données de la SAE

Mises à disposition par la Direction de la Recherche, des Études, de l’Évaluation et des Statistiques, elles regroupent les Statistiques Annuelles des Établissements, précisant des éléments liés aux activités et aux capacités de ces derniers (voir [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/sae.html) pour plus d’information).


## Structure des données

Les données finales sont structurées selon un format clé-valeur, résultant de l’agrégation des domaines de données précédemment présentés (IQSS, certification, e-Satis et SAE) et associés à leur numéro FINESS respectif. Chaque ligne du fichier clé-valeur présente ainsi une donnée issue d’un des domaines caractérisant un établissement de santé pour une année donnée (il existe de très nombreuses lignes par établissement).

Le fichier clé-valeur est complété :

-	d’un fichier de métadonnées qui documente les clés
-	d’un fichier complétant toutes les informations administratives associées à chaque FINESS
-	d’un fichier associant les autorisations d’activité pour chaque FINESS
-	d’une table de nomenclature.

Des informations complémentaires sont accessibles via [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/general-description.html).

# Processus de collecte des données

Les données brutes d’origine proviennent toutes de l’Open Data, en particulier de la plateforme data.gouv.fr (voir la section sur l’origine des sources ci-dessus). Ce sont les producteurs de ces données qui sont en charge de leur collecte annuelle directe (seuls les FINESS sont mis à jour de façon bimensuelle) auprès des établissements de santé via des plateformes numériques et de leur exposition sur les portails d’Open Data.

Différentes problématiques complexifient l’analyse longitudinale de ces données. D’une part, il existe deux types de FINESS : les FINESS géographique eux-mêmes rattachés à un FINESS juridique (avec une relation de type « plusieurs-à-un ») ; or ces relations peuvent être amenées à évoluer dans le temps sans que ce soit facilement traçable (voir la section « Evolution des FINESS » via [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/finess.html) pour plus d’information). D’autre part, les indicateurs constituant les IQSS peuvent évoluer d’une année sur l’autre, sans qu’il soit possible d’identifier leur degré de parenté.

# Pré-traitement des données

Comme décrit dans [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/ETL.html), l’ETL Scope Santé s’effectue en deux temps :

1.	pour chaque domaine – i.e. FINESS, SAE, IQSS, e-Satis, Certification –, un pipeline produit les données raffinées correspondantes suivant un processus en 3 temps :
    1.	acquisition des données en Open Data grâce aux URLs répertoriées par domaine dans le dossier `resources` (voir section [Architecture du répertoire](https://has-sante.gitlab.io/public/alpha/scope-sante-data/development.html))
    2.	traitement et agrégation des données (notamment en un fichier clé-valeur et un fichier de metadonnées les documentant)
    3.	validation des données via le [framework Frictionless](https://framework.frictionlessdata.io/) et le standard [Table Schema](https://specs.frictionlessdata.io/table-schema/).
2.	les données générées dans la précédente phase sont agrégées pour constituer le domaine scope santé.


Le détail des traitements de chaque domaine source est accessible dans les pages de documentation associées (voir les liens dans la section « Origine des données » ci-dessus). Pour ce qui est du domaine Scope Santé agrégeant les autres, une description détaillée est accessible via [ce lien](https://has-sante.gitlab.io/public/alpha/scope-sante-data/data-docs/scope-sante.html).

Les traitements ont été effectués en langage Python, et le code associé est partagé en Open Source grâce au répertoire Gitlab public [scope-sante-data](https://gitlab.com/has-sante/public/alpha/scope-sante-data). Il est à noter que les données intermédiaires générées par la première phase de l’ETL (voir description ci-dessus) ne sont pas diffusées, mais peuvent être générées grâce au code de création de la base Scope Santé.

# Diffusion du jeu de données

Les données sont diffusées publiquement grâce au portail Open Data data.gouv.fr selon la Licence Ouverte v2. Aucune redevance ou restriction n’est appliquée dans l’accès des données.

# Maintenance du jeu de données

C’est la Mission Data de la HAS joignable à [cette adresse](mailto:opendata@has-sante.fr) qui assure la maintenance du jeu de données Scope Santé. Elle est également responsable de la production, de l’éditorialisation et de la diffusion de ces dernières. Tout utilisateur de ces données peut également proposer des améliorations soit directement sur le formulaire de contact de la plateforme Data Gouv, soit en réalisant une Issue et/ou Merge Request sur Gitlab.

Il est cependant important de noter que ce sont les producteurs des données sources qui sont responsables de la production, de la diffusion, de l’éditorialisation et de la maintenance de ces dernières (les contacter au besoin). Pour plus d’information concernant la mise à jour des données, se référer à la [page suivante](https://has-sante.gitlab.io/public/alpha/scope-sante-data/maintenance.html). De façon à prévenir l’obsolescence de ces données, elles seront mises à jour de façon bimensuelle.
